'''
Frank the A.I.

Frank likes numbers and talking to humans. Give him a number and he'll help you make it better!
'''

import Friends

isRunning = True
favouriteNumber = 1.2345
friends = []

def improveNumber(myValue):
    return (myValue + favouriteNumber) ** favouriteNumber

def makeFriends():
    friends.append(Friends.Laura())
    friends.append(Friends.Sally())
    friends.append(Friends.Simon())

def makeBirthdayCard(friend):
    message = "Hello "
    message += friend.name
    message += "! "
    nextAge = int(friend.age) + 1
    message += "Happy Birthday! You're now {0} years old! - Frank.".format(str(nextAge))
    textColour = str(friend.favColour)
    message += " (I'd write it in {0} colour ink)".format(str(textColour))
    return message

while(isRunning):
    val = input("Please enter a command: ")
    if val == "quit" or val == "exit":
        isRunning = False
        continue
    elif val == "number":
        newVal = input("Please enter a number: ")
        improvedNumber = improveNumber(newVal)
        print("Here, I made that number better: " + improvedNumber)
    elif val == "birthday":
        makeFriends()
        for friend in friends:
            print(makeBirthdayCard(friend))



